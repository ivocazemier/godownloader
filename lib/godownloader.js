/**
 * Created by ivo on 19/01/14.
 */

var async = require('async');
var http = require('http');
var fs = require('fs');
var util = require('util');


// Some constants
var TYPE_TO_LOCAL_FILE = "typeFileDownloader";
var TYPE_IN_MEMORY_DOWNLOADER = "typeInMemoryDownloader";

exports.TYPE_TO_LOCAL_FILE = TYPE_TO_LOCAL_FILE;
exports.TYPE_IN_MEMORY_DOWNLOADER = TYPE_IN_MEMORY_DOWNLOADER;

// Base Class
function Downloader() {
    this.socketTimeout = 10000;
}


// Sub Class
ToLocalFileDownloader.prototype = new Downloader();

// ToLocalFileDownloader Class Constructor:
function ToLocalFileDownloader() {
    this.writeStreamOptions = {
        flags: 'w',
        encoding: null,
        mode: 0666
    };
}

ToLocalFileDownloader.prototype.unlinkOldDestination = function (destination, callBack) {


};

// ToLocalFileDownloader Methods:
ToLocalFileDownloader.prototype.download = function (options, destination, callBack) {
    var finished = false;
    var self = this; // keep ref of this.


    function unlinkFile(destination, callBack) {
        fs.exists(destination, function (exists) {

            util.log(exists ? "It's there already, unlink now..." : "No such file yet!");

            if (exists) {
                fs.unlink(destination, function (error) {
                    if (!error) {
                        callBack(null);
                    } else {
                        callBack(error);
                    }
                });
            } else {
                callBack(null);
            }
        });
    }

    async.waterfall([

        function (callBack) {

            unlinkFile(destination, function (err) {
                if (err) {
                    callBack(err);
                } else {
                    callBack(null);
                }

            });

        },
        function (callBack) {
            var stream = fs.createWriteStream(destination, self.writeStreamOptions);
            callBack(null, stream);
        },
        function (stream, callBack) {
            if (stream) {

                stream.on('open', function () {
                    util.log("Stream to file opened");
                });
                stream.on('error', function (error) {
                    util.log("Stream to file failure");
                    callBack(error);
                });


                var request = http.request(options, function (response) {


                    util.log('STATUS: ' + response.statusCode);
                    util.log('HEADERS: ' + JSON.stringify(response.headers));

                    //Another chunk of data has been received.
                    response.on('data', function (chunk) {
                        stream.write(chunk);
                    });

                    //Indicates that the underlying connection was terminated before response.end() was called or able to flush.
                    response.on('close', function () {
                        util.log("Response closed");
                        if (!finished) {
                            callBack("Response closed before we could download the rest", null);
                        }
                    });

                    //the whole response has been received
                    response.on('end', function () {
                        finished = true;
                        stream.end();
                        callBack(null, "finished download, see:'" + destination + "'");
                    });


                    response.on('error', function (err) {
                        stream.end();
                        callBack(err, null);
                    });
                });

                request.on('error', function (error) {
                    util.log("Request error");
                    callBack(error, null);
                });

                request.on('socket', function (socket) {

                    util.log("Request socket event, quickly set a timeout on it.");

                    socket.setTimeout(self.socketTimeout);


                    socket.on('timeout', function () {
                        request.abort();
                        callBack("Request socket time out..., abort request!", null);
                    });

                    socket.on('error', function(error){
                        util.log(error);
                        callBack("Error on request socket connection.", null);
                    });

                });
                request.end();
            } else {
                callBack("No stream set", null);
            }
        }
    ],
        function (err, status) {
            if (err) {
                callBack(err, null);
            } else {
                callBack(null, status);
            }
        });
}
/**
 * Factory method, returns a Downloader instance, selected by type
 * @param type
 * @param callBack
 */
exports.getDownloaderByType = function (type) {
    var result = null;

    switch (type) {
        case TYPE_TO_LOCAL_FILE:
            result = new ToLocalFileDownloader();
            break;
        case TYPE_IN_MEMORY_DOWNLOADER:
        default :
            result = null;

    }

    return result;

}

