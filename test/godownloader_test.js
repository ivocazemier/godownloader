/**
 * Created by ivo on 19/01/14.
 */

var util = require('util');
var goDownloader = require('../lib/godownloader');



var destination = "./tested_data/file.zip";

//Build a test
var downloadFile = function ( callBack) {

    var options = {
        host: "download.thinkbroadband.com",
        path: "/5MB.zip",
        port: 80,
        method: "GET"
    };

    goDownloader.getDownloaderByType(goDownloader.TYPE_TO_LOCAL_FILE, function (err, result) {
        if (!err) {
            result.download(options, destination, function (err, result) {
                if (!err) {
                    util.log("Download Result: " + result.toString());
                } else {
                    util.log(err.toString());
                }
            });
        }
    });
};

// Execute test:
downloadFile( );

